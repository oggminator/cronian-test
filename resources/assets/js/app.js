window.$ = window.jQuery = require('jquery')
require('bootstrap-sass');
require('jquery-mask-plugin');

$(document).ajaxSend(function(event, request, settings) {
    $('#loading-indicator').show();
});

$(document).ajaxComplete(function(event, request, settings) {
    $('#loading-indicator').hide();
});

$( document ).ready(function() {

	// Mask the shipping tracking code
	$( '#shipping_tracking_code' ).mask('AAAAAAAAA');

    // Bind the click event to "search_button"
    $( "#search_button" ).click(function() {
  		
  		// Get the shipping tracking code
  		var shipping_tracking_code = $( "#shipping_tracking_code" ).val();

  		// Block if the shipping tracking code was not entered
  		if( !shipping_tracking_code )
  		{
  			// Alert
	  		$( "#shipping_tracking_code_status" ).attr("class", "alert alert-danger");
	  		$( "#shipping_tracking_code_status" ).html(
				"Please provide a shipping tracking code"
	  		);
  		}
  		else
  		{
  			// Deactivate the button
  			$( "#search_button" ).attr("disabled", "disabled");
  			
  			// Search for the shipping tracking code
  			searchShippingTrackingCode(shipping_tracking_code);
  		}
	});
});

/**
 * Search for the shipping tracking code.
 *
 * @param  string  shipping_tracking_code
 * @return  string  $message
 */
function searchShippingTrackingCode(shipping_tracking_code) {

    // Make an AJAX request
    var request = $.ajax({
	    url: window.location.href + 'v1/shippingtrackingcode/' + encodeURIComponent(shipping_tracking_code),
	    headers:
	    {
	        'Accept-Language':'en_US'
	    },
	    method: 'GET',
	    dataType: 'json'
  	});

	// Request Done
	request.done(function(data)
	{
  		// Get the request response
	    var response = data.response;

	    // If it was a successfully response
	    if( response.success )
	    {
  			// Display the estimated delivery date
  			$( "#shipping_tracking_code_status" ).attr("class", "alert alert-success");
  			$( "#shipping_tracking_code_status" ).html("Your shipment (<b>" + response.results.code + "</b>) will be delivered on <b>" + response.results.estimated_delivery_date + "</b>");
	    }
	    else
	    {
	    	// Alert
  			$( "#shipping_tracking_code_status" ).attr("class", "alert alert-danger");
  			$( "#shipping_tracking_code_status" ).html(
  				getErrorMessages(response.errors)
  			);
	    }
	    
	    // Reactivate the button
  		$( "#search_button" ).removeAttr("disabled");
	});

	// Request Fail
	request.fail(function( jqXHR, textStatus ) {
  		// Alert
  		$( "#shipping_tracking_code_status" ).attr("class", "alert alert-danger");
  		$( "#shipping_tracking_code_status" ).html(
  			jqXHR.statusText
  		);

  		// Reactivate the button
  		$( "#search_button" ).removeAttr("disabled");
	});
}

/**
 * Get error messages.
 *
 * @param  object  errors
 * @return  string  $messages
 */
function getErrorMessages(errors) {

	// Store all the error messages
	var messages = [];
	// Error messages storage
	var messages_storage = [];

	// Get the error messages storage
	if( errors.route && errors.route.length )
	{
		messages_storage = errors.route;
	}
	else if( errors.validator && errors.validator.length )
	{
		messages_storage = errors.validator;
	}
	else if( errors.action && errors.action.length )
	{
		messages_storage = errors.action;
	}

	// Push all the error messages
	$.each( messages_storage, function( key, value )
	{
  		messages.push(value.message);
	});

	return messages.join("<br>");
}