<?php

namespace App\Http\Middleware\v1;

use App;
use Closure;
use Response;
use Lang;

class Api
{
    /**
     * Available API locales.
     *
     * @var array
     */
    protected static $apiLocales = [
        'en_US',
    ];

    /**
     * Selected API locale.
     *
     * @var string
     */
    protected static $apiLocale = [];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $customMessages = [];

        // Get the route
        $route = $request->route();
        
        // Get the actions of the route
        $actions = $route->getAction();

        // Get the Accept Header
        $acceptHeader = json_decode($request->header('Accept'), true);

        // Try to negotiate and decide the locale
        if( !empty($request->header('Accept-Language')) )
        {
            if( !in_array($request->header('Accept-Language'), self::$apiLocales) )
            {
                $customMessages[] = [
                    'code' => 1100,
                    'message' => 'The API requested locale is not available',
                ];
            }
            else
            {
                self::$apiLocale = explode('_', $request->header('Accept-Language'));
                
                // Set the current application locale
                App::setLocale(self::$apiLocale[0]);
            }
        }
        else
        {
            $customMessages[] = [
                'code' => 1101,
                'message' => 'The API requested header locale parameter is missing',
            ];
        }
        
        if( sizeof($customMessages) )
        {
            // Translate the custom messages
            foreach($customMessages as $customMessageIndex => $customMessage)
            {
                $customMessages[$customMessageIndex]['message'] = Lang::has('messages.'.$customMessage['message']) ? trans('messages.'.$customMessage['message']) : $customMessage['message'];
            }

            // Return a json unsuccess action negociation response
            return Response::JURN($customMessages);
        }

        // Proceed with the request
        return $next($request);
    }
}
