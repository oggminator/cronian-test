<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// API Negociation - Versioning, Locale
Route::group(['middleware' => ['api']], function()
{
	Route::get('v1/shippingtrackingcode/{code}', ['as' => 'route_shipping_tracking_code_show', 'uses' => 'v1\ShippingTrackingCodeController@show']);
});