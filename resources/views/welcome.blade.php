<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('/js/app.js') }}"></script>
    </head>
    <body>
        <div class="bg-primary" style="padding:10px; margin-bottom:10px;">
            {{ trans('texts.home.description') }}
        </div>
        <form class="form-inline">
            <div class="form-group">
                <input id="shipping_tracking_code" type="text" class="form-control" placeholder="{{ trans('texts.home.placeholder') }}" />
            </div>
            <button id="search_button" type="button" class="btn btn-primary">{{ trans('texts.home.search') }}</button>
            <div class="form-group">
                <img src="/images/hourglass.svg" id="loading-indicator" style="display:none" />
            </div>
            <p></p>
            <p>
                <i>{{ trans('texts.home.alphanumeric_characters') }}</i>
            </p>
            <div id="shipping_tracking_code_status">

            </div>
        </form>
    </body>
</html>
