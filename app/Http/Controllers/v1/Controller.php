<?php

namespace App\Http\Controllers\v1;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Request;
use Input;
use Lang;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
	 * Get the request data
	 *
	 * @return array
	 */
	protected function getRequestData()
	{
		// Determine if the request is sending JSON
		if (Request::isJson())
		{
			return $data = Input::json()->all();
		}
		// Determine if the request is sending not JSON
		else
		{
			return $data = Input::all();
		}
	}

	/**
	 * Replace all the failed validation rules of a model with the appropriate
	 * rules failed validation custom error codes available for that model
	 *
	 * @param  array  $failedValidationRules
	 * @param  array  $validationMessages
	 * @param  array  $customErrorCodes
	 * @return array
	 */
	protected function failedValidationRulesToCustomErrorCodes($failedValidationRules, $validationMessages, $customErrorCodes)
	{
		$customMessages = [];

		if (sizeof($failedValidationRules))
		{
			foreach ($failedValidationRules as $keyAttribute => $valAttribute)
			{
				$messageNo = 0;

				foreach ($valAttribute as $keyRule => $valRule)
				{
					$keyRule = strtolower($keyRule);

					// Proceed only if the custom code is available
					$customCode = !empty($customErrorCodes[$keyAttribute][$keyRule]) ? $customErrorCodes[$keyAttribute][$keyRule] : 0;
					
					if (!empty($customCode))
					{
						$customMessages[] = [
							'attribute' => $keyAttribute,
							'code' => $customCode,
							'message' => $validationMessages[$keyAttribute][$messageNo]
						];
						$messageNo++;
					}
				}
			}
		}

		return $customMessages;
	}

	/**
	 * Replace all the failed actions of a controller / model with the appropriate
	 * action custom error codes available for that model
	 *
	 * @param  array  $actionCustomErrorCodes
	 * @return array
	 */
	protected function failedActionsToCustomErrorCodes($action, $index, $actionCustomErrorCodes)
	{
		$customMessages = [];
		
		if (sizeof($actionCustomErrorCodes))
		{
			$customMessages[] = [
				'code' => $actionCustomErrorCodes[$action][$index]['code'],
				'message' => Lang::has('messages.'.$actionCustomErrorCodes[$action][$index]['message']) ? trans('messages.'.$actionCustomErrorCodes[$action][$index]['message']) : $actionCustomErrorCodes[$action][$index]['message'],
			];
		}

		return $customMessages;
	}
}
