<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application custom texts
	|--------------------------------------------------------------------------
	|
	| The following language lines are the application's custom texts
	|
	*/

	'home' =>
		[
			'description' => 'Here’s the fastest way to check the estimated delivery date of your shipment. No need to call Customer Service – our online results give you real-time, detailed progress as your shipment speeds through our network.',
			'placeholder' => 'Shipping tracking code...',
			'search' => 'Search',
			'alphanumeric_characters' => '9 alphanumeric characters',
		],
);