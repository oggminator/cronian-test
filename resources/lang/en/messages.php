<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application custom messages
	|--------------------------------------------------------------------------
	|
	| The following language lines are the application's custom messages
	|
	*/

	// Middleware
		// API
		'The API requested locale is not available' => 'The API requested locale is not available',
		'The API requested header locale parameter is missing' => 'The API requested header locale parameter is missing',

);