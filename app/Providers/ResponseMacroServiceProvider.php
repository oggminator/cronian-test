<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() 
    {

    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        |--------------------------------------------------------------------------
        | JSON Success Action Execution Response with Results
        |--------------------------------------------------------------------------
        |
        | The following response is used to returnd results if the requested action was
        | successfully executed
        |
        */

        Response::macro('JSAER', function($results = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => true,
                        'results' => $results,
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Success Action Execution Response with Data
        |--------------------------------------------------------------------------
        |
        | The following response is used to returnd data if the requested action was
        | successfully executed
        |
        */

        Response::macro('JSAED', function($data = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => true,
                        'data' => $data,
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Success Action Execution Response with Messages
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested action was
        | successfully executed
        |
        */

        Response::macro('JSAEM', function($messages = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => true,
                        'messages' => $messages,
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Unsuccess Request Route
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested route was
        | not found
        |
        */

        Response::macro('JURR', function($errors = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => false,
                        'errors' => [
                            'route' => $errors
                        ],
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Unsuccess Action Execution Response
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested action was
        | not successfully executed
        |
        */

        Response::macro('JUAE', function($errors = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => false,
                        'errors' => [
                            'action' => $errors
                        ],
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Unsuccess Action Validation Response
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested action was
        | not successfully validated
        |
        */

        Response::macro('JUAV', function($errors = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => false,
                        'errors' => [
                            'validator' => $errors
                        ],
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Unsuccess Request Negociation Response
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested action was
        | not successfully negociated by the api middleware
        |
        */

        Response::macro('JURN', function($errors = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => false,
                        'errors' => [
                            'negociation' => $errors
                        ],
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

        /*
        |--------------------------------------------------------------------------
        | JSON Unsuccess Request Access Token Owner Response
        |--------------------------------------------------------------------------
        |
        | The following response is used if the requested action was
        | not successfully negociated by the api middleware
        |
        */

        Response::macro('JURATO', function($errors = [])
        {
            return Response::json(
                [
                    'response' => [
                        'success' => false,
                        'errors' => [
                            'accessTokenOwner' => $errors
                        ],
                    ],
                ],
                200,
                [],
                JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        });

    }

}