<?php

use Illuminate\Database\Seeder;

use App\Models\v1\ShippingTrackingCode;

class ShippingTrackingCodesSeeder extends Seeder
{
    /**
     * Shipping Tracking Codes.
     *
     * Alpha Numeric and 9 chars long strings.
     */
    private static $shippingTrackingCodes = [
	    [
		    'code' => 'sZ8ZSBXWw',
	    ],
	    [
		    'code' => '8qE7UN6bS',
	    ],
	    [
		    'code' => 'VW6PtWCqD',
	    ],
	    [
		    'code' => 'xzChm5e8a',
	    ],
	    [
		    'code' => 'qLV2Teh6E',
	    ],
	    [
		    'code' => 'fY9Str5V3',
	    ],
	    [
		    'code' => '5CrK2uc6T',
	    ],
	    [
		    'code' => 'z7mJkTmkk',
	    ],
	    [
		    'code' => 'ivbEDVnUS',
	    ],
	    [
		    'code' => 'syA3irmwc',
	    ],
	    [
		    'code' => 'D0ZejhtfC',
	    ],
	    [
		    'code' => 'P0ha9Vgfy',
	    ],
	    [
		    'code' => 'lf3RDzwqF',
	    ],
	    [
		    'code' => 'xTGgN9gVR',
	    ],
	    [
		    'code' => 'Kw7UpG2UT',
	    ],
	    [
		    'code' => 'LopmSMxK6',
	    ],
	    [
		    'code' => '7utHGSgTf',
	    ],
	    [
		    'code' => 't5G9AXaVV',
	    ],
	    [
		    'code' => 'wEMEWluBZ',
	    ],
	    [
		    'code' => 'SJIMmT2fo',
	    ],
	    [
		    'code' => 'iKfBeFHii',
	    ],
	    [
		    'code' => '6qbUBt7Ac',
	    ],
	    [
		    'code' => 'nDTEJNpR0',
	    ],
	    [
		    'code' => 'DLNhqW9mP',
	    ],
	    [
		    'code' => '9jsIRJlJb',
	    ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Drop the existing "shipping_tracking_codes" table
        DB::table('shipping_tracking_codes')->delete();

        // Seed the "shipping_tracking_codes" table
        foreach(self::$shippingTrackingCodes as $shippingTrackingCode)
        {
        	ShippingTrackingCode::create($shippingTrackingCode);
        }
    }
}
