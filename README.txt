Pasii efectuati pentru realizarea testului:

1. laravel new croniantest - am initializat un nou proiect Laravel

2. Am actualizat fisierul .env cu datele pentru conectare pentru cele 2 solutii de stocare: SQLite si MySQL

3. Schimbul intre cele 2 solutii de stocare se face prin setarea variabilei DB_CONNECTION din fisierul .env fie la "mysql" fie la "sqlite"

4. Am comentat toate liniile din fisierul .gitignore pentru a putea adauga in Git toate pachetele / modulele / fisierele de configurare etc. In mod normal, ceea ce se gaseste in .gitignore nu se comite in Git. Am facut acest lucru insa pentru usurarea etapei de testare (dupa clonarea proiectului, tot ceea ce trebuie sa faceti este sa creati un vhost, sa va asigurati ca exista bazele de date in cele 2 solutii de stocare, sa rulati migratiile si seeder-ele si sa accesati proiectul din browser)

5. Pentru realizarea API-ului REST am facut urmatoarle:

	a. am mutat toate controalerele, modelele, middleware-urile sub namespace-ul \v1 corespunzator fiecaruia in parte. Acest lucru ajuta la realizarea versionarii API-ului
	b. Am creat middleware-ul Api.php responsabil de Localization API-ului (Accept-Language header : en_US)
	c. am creat ruta cu verbul GET specifica unui call REST API de show la care am adaugat header-ul mai sus mentionat, url-ul care include versionarea si codul de comanda care se doreste a fi cautat (vedeti colectia POSTMAN din directorul postman din proiect)
	d. am actualizat controller-ul principal (extins de celelalte controllere) cu functionalitati de preluare a datelor trimise si de generare mesaje / raspunsuri personalizate
	e. Am creat si inregistrat un Service Provider (ResponseMacroServiceProvider.php) responsabil pentru returnarea unei suite de raspunsuri particularizate
	f. Am actualizat Handler-ul de Exceptii pentru a prinde exceptiile de tipul NotFoundHttpException (in cazul unui call API REST inexistent - care nu are o ruta setata)

6. Am creat migratia pentru tabela unde sunt stocate codurile

7. Am generat o suita de coduri aleatoare si le-am inclus in fisierul de seed folosit pentru popularea bazei de date cu coduri

8. Am creat controlerul si modelul pentru coduri

9. Pentru partea de front-end am facut urmatoarele:

	a. am instalat modulele npm necesare (Laravel Elixir, Bootstrap Sass, Gulp, jquery, jquery-mask-plugin, babel-preset-es2015, babel-preset-react)
	b. am adaugat Bootstrap JavaScript folosind Browserify
	c. am implementat functionalitatile JavaScrip in resources/assets/js/app.js
	d. Call-ul REST API se realizeaza prin AJAX
	e. am actualizat view-ul principal al aplicatiei cu implementarile necesare proiectului
	f. folosind Gulp am publicat toate asset-urile necesare din directorul resources in public

10. Proiectul are suport pentru Localization (multilanguage):

	a. Atat in view-uri cat si in raspunsurile API-ului mesajele sunt returnate in functie de header-ul Accept-Language header : en_US trimis. In acet moment exista suport doar pentru en, dar se pot adauga cu usurinta limbi suplimentare.


Nota: Proiectul trebuie clonat, verificat / actualizat setarile din .env, rulat migratiile si seed-erele si accesat in URL.

	Toate modulele, pachetele, vendor etc sunt deja configurate (nu se va mai rula composer install, nmp install, gulp, etc)

Multumesc