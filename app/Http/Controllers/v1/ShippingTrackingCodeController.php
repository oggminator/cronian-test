<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;

use App\Http\Requests;

// Load Models
use App\Models\v1\ShippingTrackingCode;

use Validator;
use Response;
use Exception;

class ShippingTrackingCodeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  string  $code
     * @return Response
     */
    public function show(Request $request, $code = null)
    {
        // Get the request data
        $data = $this->getRequestData();

        // Add the code param
        $data['code'] = urldecode($code);
        
        // Validate the request data
        $validator = Validator::make($data, ShippingTrackingCode::$rules['show']);

        if ($validator->fails())
        {
            $errors = $this->failedValidationRulesToCustomErrorCodes(
                $validator->failed(),
                $validator->messages()->getMessages(),
                ShippingTrackingCode::$rulesFailedValidationCustomErrorCodes['show']
            );

            // Return a json unsuccess action validation response
            return Response::JUAV($errors);
        }

        // Try to show a document
        try
        {
            $shippingTrackingCode = ShippingTrackingCode::getByCode($data['code']);
            
            if( !$shippingTrackingCode )
            {
                throw new Exception('not_found');
            }
        }
        catch (Exception $e)
        {
            $errors = $this->failedActionsToCustomErrorCodes('show', $e->getMessage(), shippingTrackingCode::$actionsCustomErrorCodes);
            
            // Return a json unsuccess action execution response
            return Response::JUAE($errors);
        }

        // Return a json success action execution response with results
        return Response::JSAER(
            $shippingTrackingCode
        );
    }
}
