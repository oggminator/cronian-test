<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Seed the OAuth Clients Table
        $this->call(ShippingTrackingCodesSeeder::class);

        Model::reguard();

        // Data Successfully Seeded
        $this->command->info('Data Successfully Seeded!'); 
    }
}
