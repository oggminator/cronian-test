<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

use DateTime;
use DateInterval;

class ShippingTrackingCode extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_tracking_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'estimated_delivery_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    // Rules
    public static $rules = [
        // show
        'show' => [
            'code' => 'required|string|alpha_num|size:9',
        ],
    ];

    // Rules failed validation custom error codes
    public static $rulesFailedValidationCustomErrorCodes = [
        // show
        'show' => [
            'code' => [
                'required' => 1001,
                'string' => 1002,
                'alpha_num' => 1003,
                'size' => 1004,
            ],
        ],
    ];

    // Custom error codes
    public static $actionsCustomErrorCodes = [
        // show
        'show' => [
            'not_found' => [
                'code' => 2001,
                'message' => 'The shipping tracking code was not found',
            ],
        ],
    ];

    /**
     * Get by code
     *
     * @param  string  $code
     * @return object
     */
    public static function getByCode($code)
    {
        return ShippingTrackingCode::where('code', $code)->first();
    }

    /**
     * Calculate the estimated delivery date
     *
     * @return string
     */
    public static function getEstimatedDeliveryDateAttribute()
    {
        // Get the current date
        $date = new DateTime();

        // Generate a random number between 0 and 31
        $days = mt_rand(0, 31);

        // Add the number of days to the current date
        $date->add(new DateInterval('P'.$days.'D'));
        
        // Return the estimated delivery date
        return $date->format('l, j F Y ');
    }    

    /**
     * Get the created_at field.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return strtotime($value);
    }

    /**
     * Get the updated_at field.
     *
     * @param  string  $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return strtotime($value);
    }
}
